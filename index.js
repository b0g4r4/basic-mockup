const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const routes = require('./src/http/route/route');
const CORSMiddleware = require('./src/http/middleware/CORS.middleware');
const responseMiddleware = require('./src/http/middleware/response.middleware');

// middleware
app.use(bodyParser.json())
app.use(CORSMiddleware);

// config
app.set('json spaces', 2)

// logic
app.use('/api', routes);

// middleware
app.use(responseMiddleware);

app.listen(3000, () => {
  console.log("Server is listening on port 3000");
});