const express = require('express');
const authController = require('../../api/auth/auth.controller');
const { requestValidatorMiddleware } = require('../middleware/requestValidator.middleware');

const router = express.Router();

const ac = new authController();
router.post('/pendaftaran', requestValidatorMiddleware, ac.createPendaftaran);

module.exports = router
