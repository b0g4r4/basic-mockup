class AuthController {
    async createPendaftaran(req, res, next) {
        let dataResponse = {}
        if (req.error.length > 0) {
             dataResponse = {
                "code": 422,
                "message": "Data Bermasalah",
                "entity": "index",
                "state": "indexUnprocessableEntity",
                "error": req.error
                }
            req.businessLogic = dataResponse
            next()
            return
        }
        dataResponse = {
            "code": 201,
            "message": "Berhasil",
            "entity": "index",
            "state": "indexSuccess"
            }
        req.businessLogic = dataResponse
        next()
    }

    async createPendaftaran2(req, res) {
        let dataResponse = {}
        if (req.error.length > 0) {
             dataResponse = {
                "code": 422,
                "message": "Data Bermasalah",
                "entity": "index",
                "state": "indexUnprocessableEntity",
                "error": req.error
                }
            res.status(dataResponse.code).send(dataResponse)
        }
        dataResponse = {
            "code": 201,
            "message": "Berhasil",
            "entity": "index",
            "state": "indexSuccess"
            }
        res.status(dataResponse.code).send(dataResponse)
    }
}



module.exports = AuthController;




















// app.get('/', (req, res) => {
//     var dataResponse = {
//       "message": "Berhasil",
//       "entity": "index",
//       "state": "indexSuccess"
//     }
//     res.header("Content-Type",'application/json');
//     res.send(dataResponse)
// });

// app.get('/pendaftaran', (req, res) => {
//     var dataResponse = {
//       "message": "Pendaftaran Berhasil",
//       "entity": "registration",
//       "state": "registrationSuccess"
//     }
//     res.header("Content-Type",'application/json');
//     res.send(dataResponse)
// });

// app.post('/pendaftaran', (req, res) => {
//     console.log(req.body.nama)
//     if (req.body === {} || req.body === undefined) {
//         var dataResponse = {
//             "message": "Data Bermasalah",
//             "entity": "registration",
//             "state": "invalidAttributes"
//           }
//           res.header("Content-Type",'application/json');
//           res.send(dataResponse)  
//           return
//     }
//     var dataResponse = {
//       "message": "Pendaftaran Berhasil",
//       "entity": "registration",
//       "state": "registrationSuccess"
//     }
//     res.header("Content-Type",'application/json');
//     res.send(dataResponse)
// });